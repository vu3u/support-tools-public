#!/usr/bin/env bash

# ntfsclone wrapper
# ver=0.3
# Have a question? Contact OnApp Support Team

# init()
NTFSCLONE=$(which ntfsclone)
NTFSCLONE_PATH=${NTFSCLONE%/*}
NTFSCLONE_ORIG="${NTFSCLONE_PATH}/ntfsclone.orig"
DD=$(which dd)

MODE=0 # Backup, MODE=1 provisioning, MODE=2 restoration 
#PROV=false

# Variables that are allowed to change
TMPL_PATH='/onapp/templates/'
BKP_PATH='/onapp/backups/'
DD_BS='bs=64MB'
SPARSE=false # true/false. It affects modes 1,2 only. The backup mode is always sparse
COMPR=true # true/false. Comprassion control. Then sparse option is not used 

#BASH_ARGV An array variable containing all of the parameters in the current bash execution call stack. The final parameter of the last subroutine call is at the top of the stack
#example ntfsclone -q -s -O /onapp/backups/d/t/dtapxprpuudixn /dev/sde1 
DD_IN=${BASH_ARGV[0]} #/dev/sde1
DD_OUT=${BASH_ARGV[1]} #/onapp/backups/d/t/dtapxprpuudixn
DD_CONV=''
DD_IFLAG='iflag=fullblock'

${SPARSE} && DD_CONV='conv=sparse'

DD_OPTIONS="${DD_BS} ${DD_IFLAG}"

if which pigz >/dev/null 2>&1; then
  COMP_TOOL=$(which pigz) # much more effective than gzip
  UNCOMP_TOOL=$(which unpigz)
else
  COMP_TOOL=$(which gzip)
  UNCOMP_TOOL=$(which gunzip)
fi

# main()
if [[ ! -f ${NTFSCLONE_ORIG} ]]; then
   echo "${NTFSCLONE_ORIG} binary file doesn't exist"
   exit 1
fi

if [[ "$#" -eq 0 ]]; then
   echo "No arguments specified"
   exit 1
fi

# Determining the current mode

# provisioning mode
if echo ${DD_OUT} | grep -q "/dev/" && echo ${DD_IN} | grep -q "${TMPL_PATH}"; then
   MODE=1
   if ${SPARSE}; then
      DD_OPTIONS+=" ${DD_CONV}"
   fi
 fi
# restoration mode
if echo ${DD_OUT} | grep -q "/dev/"  && echo ${DD_IN} | grep -q "${BKP_PATH}" ; then
   MODE=2
   if ${SPARSE}; then
      DD_OPTIONS+=" ${DD_CONV}"
   fi
fi

echo "Warning! The ntfsclone wrapper is running."

# backing up;
if [[ $MODE -eq 0 ]]; then
      if ${COMPR}; then
         echo "Running: ${DD} if=${DD_IN} ${DD_OPTIONS} | ${COMP_TOOL} -c - > ${DD_OUT}" 
         ${DD} if=${DD_IN} ${DD_OPTIONS} | ${COMP_TOOL} -c - > ${DD_OUT}
      else
         echo "Running: ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS} conv=sparse"
         ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS} conv=sparse
      fi
  exit $?
fi
# end backing up

# restoring
if [[ $MODE -eq 2 ]]; then
  # backup files were created with ntfsclone
  if head -n1 ${DD_IN} | grep -q -w "ntfsclone-image" ; then
    echo "Warning! A ntfsclone backup detected. Original ntfsclone tool is running."
    ${NTFSCLONE_ORIG} "$@"
  
  # assuming that backups were created by the wrapper
  elif file -s ${DD_IN} | awk '{print $2}' | grep -q -w "gzip"; then
    echo "Running: ${UNCOMP_TOOL} -c ${DD_IN} | ${DD} ${DD_OPTIONS} of=${DD_OUT}"
    mv ${DD_IN} ${DD_IN}.gz
    ${UNCOMP_TOOL} -c ${DD_IN}.gz | ${DD} ${DD_OPTIONS} of=${DD_OUT}
    mv ${DD_IN}.gz ${DD_IN}
  else
    echo "Running: ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}"
    ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}
  fi
  exit $?
fi
# end restoring

# provisioning
if [[ $MODE -eq 1 ]]; then
   #onapp tamplates or custom templates that were created with ntfsclone;
    if echo ${DD_IN} | grep -q "${TMPL_PATH}win" || head -n1 ${DD_IN} | grep -q -w "ntfsclone-image"; then
      echo "Warning! An onapp ntfsclone template detected. Original ntfsclone tool is running."
      ${NTFSCLONE_ORIG} "$@"
    
    #custom templates; assuming that they were created with dd 
    elif file -s ${DD_IN} | awk '{print $2}' | grep -q -w "gzip"; then
      #echo "Warning! This is a custom template that was created by the ntfsclone wrapper. The ntfsclone wrapper is running."
      echo "Running: ${UNCOMP_TOOL} -c ${DD_IN} | ${DD} ${DD_OPTIONS} of=${DD_OUT}"
      mv ${DD_IN} ${DD_IN}.gz
      ${UNCOMP_TOOL} -c ${DD_IN}.gz | ${DD} ${DD_OPTIONS} of=${DD_OUT}
      mv ${DD_IN}.gz ${DD_IN}
    else
      echo "Running: ${DD} if=${DD_IN} of=${DD_OUT} bs=${DD_BS} ${DD_CONV}"
      ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}
    fi
  exit $?
fi
# end provisioning

#If the above doesn't meet any conditions

${NTFSCLONE_ORIG} "$@"
exit $?
