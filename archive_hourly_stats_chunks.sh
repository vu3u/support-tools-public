#!/usr/bin/bash

# The script deletes old hourly stats from billing_usage_statistics, billing_statistics_hourly tables in chunks
# @A.K 
# 
### INIT() ###

[ -d '/onapp/interface/' ] || exit 1

#Log file
LOG_FILE=/onapp/interface/log/archive_hourly_stats_chunks-$(date +%F_%H-%M-%S).log

SCRIPTNAME=${0##*/}
echo -e "$(date)\n" > $LOG_FILE

#Get Number of mounths to keep stats
STATS_KEEP_MONTH=`awk '$1=="archive_stats_period:"{print $2}' /onapp/interface/config/on_app.yml`

# Getting credentials from database.yml
DB_CONF="/onapp/interface/config/database.yml"
DB_NAME=`awk '$1=="database:"{print $2}' $DB_CONF`
DB_USER=`awk '$1=="username:"{print $2}' $DB_CONF`
DB_PASS=`awk '$1=="password:"{print $2}' $DB_CONF`
DB_HOST=`awk '$1=="host:"{print $2}' $DB_CONF`
DB_PORT=`awk '$1=="port:"{print $2}' $DB_CONF`

TABLES=("billing_usage_statistics" "billing_statistics_hourly")

#default
CHUNK_SIZE=1000000
DELAY=2

### MAIN() ###

# Get right number of months to keep stats: STATS_KEEP_MONTH + current month
((STATS_KEEP_MONTH++))

ARCHIVE_DATE=$(date "+%F %H:%M:%S" --date="$(date +'%Y-%m-01') - $(( STATS_KEEP_MONTH - 1 )) month - 1 second")

echo -e "Job: Delete all records earlier or equal to ${ARCHIVE_DATE}\n" >> $LOG_FILE

GET_MIN_ID(){
 TABLE_NAME=$1
 mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -Bse "SELECT MIN(id) FROM ${TABLE_NAME} WHERE stat_time <= '${ARCHIVE_DATE}';" 2>/dev/null
}

GET_NEXT_MIN_ID(){
 TABLE_NAME=$1; LIMIT=$2; MIN_ID=$3
 mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -Bse "SELECT id FROM ${TABLE_NAME} WHERE id >= ${MIN_ID} stat_time <= '${ARCHIVE_DATE}' ORDER BY id LIMIT ${LIMIT},1;" 2>/dev/null
}


PARAMS="$@"
#Parsing options
while (( "$#" )); do
    case "$1" in
        -l) CHUNK_SIZE="$2"; shift 2 ;;
        -dryrun) DRY_RUN=true; shift ;;
        -d) DELAY="$2"; shift 2 ;;
        *) shift ;;
  esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

echo -e "Chunk size: ${CHUNK_SIZE}" >> $LOG_FILE
echo -e "Delay: $DELAY" >> $LOG_FILE

if [[ "X${DRY_RUN}" == "Xtrue" ]]; then
  echo -e "This is the dry run mode. No changes to the DB\n" >> $LOG_FILE
fi

for T in "${TABLES[@]}"; do
  STARTTIME=$(date +%s)
  MIN_ID=$(GET_MIN_ID ${T})
  echo -e "\nProcessing the table $T" >> $LOG_FILE
  TOTAL_ROWS_TO_DEL=$(mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -Bse "SELECT count(id) FROM ${T} WHERE stat_time <= '${ARCHIVE_DATE}';")
  echo "Total rows to delete: ${TOTAL_ROWS_TO_DEL}" >> $LOG_FILE
  [[  ${TOTAL_ROWS_TO_DEL} -le 0 ]] && continue
  i=0
  while true; do
    NEXT_MIN_ID=$(mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -Bse "SELECT id FROM ${T} WHERE id >= ${MIN_ID} and stat_time <= '${ARCHIVE_DATE}' ORDER BY id LIMIT ${CHUNK_SIZE},1;")
      if [[ -z ${NEXT_MIN_ID} ]]; then
        echo 'Almost ready...' >> $LOG_FILE
        break
    fi
    if [[ "X${DRY_RUN}" == "Xtrue" ]]; then
       echo -e "DRY: DELETE FROM ${T} WHERE id >= ${MIN_ID} AND id < ${NEXT_MIN_ID} AND stat_time <= '${ARCHIVE_DATE}" >> $LOG_FILE 2>&1
      else
       mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -e  "DELETE FROM ${T} WHERE id >= ${MIN_ID} AND id < ${NEXT_MIN_ID} AND stat_time <= '${ARCHIVE_DATE}';" >> $LOG_FILE 2>&1
    fi
    MIN_ID=${NEXT_MIN_ID}
    ((i++))
    sleep "${DELAY}"
  done
  #processing the last chunk.
  echo "Processing the last chunk" >> $LOG_FILE
  if [[ "X${DRY_RUN}" == "Xtrue" ]]; then
     echo -e "DRY: DELETE FROM ${T} WHERE id >= ${MIN_ID} AND stat_time <= '${ARCHIVE_DATE}'" >> $LOG_FILE 2>&1
  else
     mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -e "DELETE FROM ${T} WHERE id >= ${MIN_ID} AND stat_time <= '${ARCHIVE_DATE}';" >> $LOG_FILE 2>&1
  fi
  echo -e "Iterations: $((i++))" >> $LOG_FILE
  ENDTIME=$(date +%s)
  ((SEC=$ENDTIME-$STARTTIME, MIN=SEC/60))
  echo -e "It took ${MIN} minute(s) to complete\n" >> $LOG_FILE
  echo -e "Current state:" >> $LOG_FILE
  mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -e "SELECT COUNT(id), MIN(stat_time), MAX(stat_time) FROM ${T};" >> $LOG_FILE 2>&1
done

echo -e "\n$(date)" >> $LOG_FILE
exit 0
