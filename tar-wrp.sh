#!/usr/bin/env bash

###
## tar wrapper
## ver=0.2
## andiry.kuzma@onapp.com
## This script comes as is and without any warranty.
## Despite it was tested please perform a series of tests on your cloud before putting it in production.
## Feel free to modify it depending on your needs.
####

##Configurable variables
TMPL_PATH='/onapp/templates/'
BKP_PATH='/onapp/backups/'
DD_BS='8M'
#The backup mode is always sparse unless compression is enabled.
SPARSE=false # [true|false]
#Compression control. It affects only backup mode as other modes are auto-detected.
COMPR=true # [true|false]
#Bypass the wrapper and run native tar.
WRP_ENABLED=true # [true|false]
##


# init()

TAR=$(which tar)
TAR_PATH=${TAR%/*}
TAR_ORIG="${TAR_PATH}/tar.original"
DD=$(which dd)

if which pigz >/dev/null 2>&1; then
    COMP_TOOL=$(which pigz)
    UNCOMP_TOOL=$(which unpigz)
    PIGZIP=true
else
	COMP_TOOL=$(which gzip)
	UNCOMP_TOOL=$(which gunzip)
fi

[[ ! -d "${TMPL_PATH}" ]] && ( echo -e "${TMPL_PATH} is not a directory. Check script configuration settings." && exit 1)
[[ ! -d "${BKP_PATH}" ]] && ( echo -e "${BKP_PATH} is not a directory. Check script configuration settings." && exit 1)


if [[ ! -f ${TAR_ORIG} ]]; then
	 echo "${TAR_ORIG} binary file doesn't exist"
	 exit 1
fi

if [[ "$#" -eq 0 ]]; then
	 echo "No arguments specified"
	 exit 1
fi

##Compression tool init
try_init_pigz_tool()
{
    echo -e "\nTrying to install pigz."
    echo -e "Running: yum install pigz -y > /dev/null 2>&1"
        if  yum install pigz -y > /dev/null 2>&1 ;then
            COMP_TOOL=$(which pigz)
            UNCOMP_TOOL=$(which unpigz)
            echo -e "pigz: Installed.\n"
        else
            echo -e "pigz: Failed to install. Try to install pigz manually."
	    fi
}
##

############
## main() ##
############

echo -e "Warning! The tar wrapper is running."

PARAMS="$@"
#Parsing tar arguments
while (( "$#" )); do
  	case "$1" in
        -f ) RUN_MODE="backup"; TAR_DST_FILE="$2"; shift 2 ;;
        -xzpf) TAR_SRC_FILE="$2"; shift 2 ;;
		-zcp) RUN_MODE="backup"; shift ;;
		-C) TAR_DIR_PATH="$2"; shift 2 ;;
		*) shift ;;
	esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"
#echo -e "TAR args (after parsing): $@"

## Determining other RUN modes
if [[ "${TAR_SRC_FILE}" =~ ^"${TMPL_PATH}".* ]];then
	RUN_MODE="provisioning"
fi

if [[ "${TAR_SRC_FILE}" =~ ^"${BKP_PATH}".* ]];then
	RUN_MODE="restoring"
fi


if [[ "${TAR_DIR_PATH}" =~ ^"${BKP_PATH}/templates/".* && "${TAR_SRC_FILE}" =~ ^"${TMPL_PATH}".* ]];then
	RUN_MODE="extract"
fi


if [[ "x${WRP_ENABLED}" != "xtrue" &&  "${RUN_MODE}" == "backup" ]]; then
	echo "The tar wrapper is disabled for backup taking. Running native tar."
	${TAR_ORIG} "$@"
	exit $?
fi


[[ "${RUN_MODE}" == "backup" &&  "${COMPR}" == "true" && "x${PIGZIP}" != "xtrue" ]] && try_init_pigz_tool


#Find the device name. The partiotion will be backed up (the one which is mounted). 
if [[ ! "${RUN_MODE}" == "extract" ]]; then
	
	DEV_NAME=$(df --output=source "${TAR_DIR_PATH}" 2>/dev/null| tail -n1)

	### It might be we need to consider using the parent device instead of its partition 

fi

##Generating DD arguments
if [[ "${RUN_MODE}" == "backup" ]];then
	DD_IN="${DEV_NAME}"
	DD_OUT="${TAR_DST_FILE}"
	DD_CONV="sparse"
elif [[ "${RUN_MODE}" == "provisioning" || "${RUN_MODE}" == "restoring" ]]; then
	DD_OUT="${DEV_NAME}"
	DD_IN="${TAR_SRC_FILE}"
fi

DD_IFLAG='fullblock'

[[ "${SPARSE}" == "true" ]] && DD_CONV='sparse' || DD_CONV=''
[[ "${RUN_MODE}" == "backup" && "${COMPR}" == "false" ]] && DD_CONV='sparse'
[[ "${RUN_MODE}" == "backup" && "${COMPR}" == "true" ]] && DD_CONV=''
[[ "${RUN_MODE}" != "backup" &&  "x${DD_CONV}" != "x" ]] && DD_CONV+=',fdatasync'
[[ "${RUN_MODE}" != "backup" &&  "x${DD_CONV}" == "x" ]] && DD_CONV+='fdatasync'
[[ "x${DD_IFLAG}" != "x" ]] && DD_IFLAG="iflag=${DD_IFLAG}"
[[ "x${DD_CONV}" != "x" ]] && DD_CONV="conv=${DD_CONV}"
[[ "x${DD_BS}" == "x" ]] && DD_BS="bs=8M" || DD_BS="bs=${DD_BS}"

DD_OPTIONS="${DD_BS} ${DD_IFLAG} ${DD_CONV}"
##

echo -e "Running mode: ${RUN_MODE}\n"

## Do backup
if [[ $RUN_MODE == "backup" ]]; then
	echo -e "Running: umount ${DEV_NAME}"
	umount "${DEV_NAME}"
	sleep 1
			if ${COMPR}; then
				 echo -e "Running: ${DD} if=${DD_IN} ${DD_OPTIONS} | ${COMP_TOOL} -c - > ${DD_OUT}\n" 
				 ${DD} if=${DD_IN} ${DD_OPTIONS} | ${COMP_TOOL} -c - > ${DD_OUT}
			else
				 echo -e "Running: ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}\n"
				 ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}
			fi
	r=$?
	echo -e "Running: mount ${DEV_NAME} ${TAR_DIR_PATH}"
	mount "${DEV_NAME}" "${TAR_DIR_PATH}"
	sleep 1
	exit "${r}"
fi
## end backing up

## Do restoring
if [[ "${RUN_MODE}" == "restoring" ]]; then
	#check it's a compressed file
	if  file -s ${DD_IN} | awk '{print $2}' | grep -q -w "gzip"; then
		#check the file is a tarball. Original tar is used.
		if  [[ `${TAR_ORIG} -tf ${DD_IN} 2> /dev/null | wc -l` -ne 0 ]] ; then
			echo -e "It seems it's a tar archive. Running native tar.\n"
			echo -e "Running: ${TAR_ORIG} $*\n"
			${TAR_ORIG} "$@"
			r=$?
		else
			echo -e "Running: umount ${DEV_NAME}"
			umount "${DEV_NAME}"
			sleep 1
			#assuming that backups were created by the wrapper with compression
			echo -e "Running: ${UNCOMP_TOOL} -c ${DD_IN} | ${DD} of=${DD_OUT} ${DD_OPTIONS}\n"
			${UNCOMP_TOOL} -c ${DD_IN} | ${DD} of=${DD_OUT} ${DD_OPTIONS}
			r=$?
			## unzip directly on the device
			#echo -e "Running: ${UNCOMP_TOOL} -c ${DD_IN} > ${DD_OUT}\n"
			#${UNCOMP_TOOL} -c ${DD_IN} > ${DD_OUT}
			echo -e "Running: mount ${DEV_NAME} ${TAR_DIR_PATH}"
			mount "${DEV_NAME}" "${TAR_DIR_PATH}"
			sleep 1
		fi	
	else
		#assuming it's a raw dd backup and without compression.
		echo -e "Running: umount ${DEV_NAME}"
		umount "${DEV_NAME}"
		sleep 1
		echo -e "Running: ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}\n"
		${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}
		r=$?
		echo -e "Running: mount ${DEV_NAME} ${TAR_DIR_PATH}"
		mount "${DEV_NAME}" "${TAR_DIR_PATH}"
		sleep 1
	fi
	exit "${r}"
fi
## end restoring

## provisioning
if [[ "${RUN_MODE}" == "provisioning" ]]; then
	
	#assuming it's an original onapp tamplate or custom templates that were created with native tar.
		if [[ "${DD_IN}" =~ .*".tar.gz" ]]; then
			echo -e "An onapp template detected. Running native tar."
			echo -e "Running: ${TAR_ORIG} $*\n"
			${TAR_ORIG} "$@"
			r=$?
		#custom templates
		elif file -s ${DD_IN} | awk '{print $2}' | grep -q -w "gzip"; then
			#assuming that they were created with native tar 
			if  [[ `${TAR_ORIG} -tf ${DD_IN} 2> /dev/null | wc -l` -ne 0 ]] ; then
					echo -e "It seems it's a tar archive.\n"
					echo -e "Running: ${TAR_ORIG} $*\n"
					${TAR_ORIG} "$@"
					r=$?
			else
				#assuming that they were created with dd and compressed 
				echo -e "Running: umount ${DEV_NAME}"
				umount "${DEV_NAME}"
				sleep 1
				echo -e "Running: ${UNCOMP_TOOL} -c ${DD_IN} | ${DD} of=${DD_OUT} ${DD_OPTIONS}\n"
				${UNCOMP_TOOL} -c ${DD_IN} | ${DD} of=${DD_OUT} ${DD_OPTIONS}
				r=$?
				## unzip directly on the device
				#echo -e "Running: ${UNCOMP_TOOL} -c ${DD_IN} > ${DD_OUT}\n"
				#${UNCOMP_TOOL} -c ${DD_IN} > ${DD_OUT}
				echo -e "Running: mount ${DEV_NAME} ${TAR_DIR_PATH}"
				mount "${DEV_NAME}" "${TAR_DIR_PATH}"
				sleep 1
			fi
		else
			#assuming that they were created with dd and without compression
			echo -e "Running: umount ${DEV_NAME}"
			umount "${DEV_NAME}"
			sleep 1
			echo -e "Running: ${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}\n"
			${DD} if=${DD_IN} of=${DD_OUT} ${DD_OPTIONS}
			r=$?
			echo -e "Running: mount ${DEV_NAME} ${TAR_DIR_PATH}"
			mount "${DEV_NAME}" "${TAR_DIR_PATH}"
			sleep 1
		fi
	r=$?
	exit "${r}"	
fi
## end provisioning

##If the above doesn't meet any conditions
echo -e "No proper wrapper conditions found. Running native tar."
echo -e "Running: ${TAR_ORIG} $*\n"
${TAR_ORIG} "$@"
exit $?
