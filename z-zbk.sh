#!/bin/bash

PROGNAME=${0##*/}
VERSION="3.7b"
CURR_INTERPRETER=$(/bin/sed -r -e 's/\x0.*//' /proc/$$/cmdline)
debug=false

#Authors: Matt & Zol(adding a few features/inprovments)

#Someone might try to run this for several backup servers at once.
#It might be a good feature to allow for parallel removal, however, as it is
#this will not work.
# Zol - will try to implement this



LOCKFILE=/tmp/zbk.lock

#Make sure only one instance of zbk.sh is running
if [ -e $LOCKFILE ]; then
        echo "$PROGNAME is already running. ($LOCKFILE found)"
        echo Please allow it to finish before starting another instance.
        echo Exiting.
        exit
fi

touch $LOCKFILE


# Color constants for prettying output
NOCOLOR='\e[0;39m'
BLACK='\e[0;30m'
RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
BLUE='\e[0;34m'
MAGENTA='\e[0;35m'
CYAN='\e[0;36m'
LTGRAY='\e[0;37m'
GRAY='\e[0;90m'
LTRED='\e[0;91m'
LTGREEN='\e[0;92m'
LTYELLOW='\e[0;93m'
LTBLUE='\e[0;94m'
LTMAGENTA='\e[0;95m'
LTCYAN='\e[0;96m'
WHITE='\e[0;97m'


##########
# FUNCTIONS
##########

clean_up() {
	rm -f $LOCKFILE
        rm -f $TMPFILE
        return
}

error_exit() {
        echo -e "${PROGNAME}: ${1:-"Unknown Error"}" >&2
        clean_up
        exit 1
}


graceful_exit() {
        clean_up
        exit
}

signal_exit() { # Handle trapped signals
        case $1 in
                INT)    error_exit "y u do dis?" ;;
                TERM)   echo -e "$PROGNAME: Program terminating" >&2 ; graceful_exit ;;
                *)      error_exit "$PROGNAME: Terminating on unknown signal" ;;
        esac
}

usage() {
        echo -e "Usage: bash $PROGNAME [OPTION] [PARAMETER]"
}

help_message() {
cat << EOF
        $PROGNAME ver $VERSION
        Clean zombie backups

        $(usage)

        Options:
        -B [IPADDRESS]          Remove all zombie backups on specified Backup Server
        -C                      Remove all zombie backups on Control Panel
        -H [IPADDRESS]          Remove all zombie backups on Hypervisor
        -s			            Enable size reporting (slower)
        -h                      Display this help message and exit
        -p                      Check running backup processes
        -M                      Check for missing backup files
        -d                      Debug mode
        -i                      Info mode
EOF
        return
}

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

function find_zombies()
{
        echo -e "${WHITE}Searching for zombies on CP${NOCOLOR}"
	totalsize=0

        #For each backup on disk, check every identifier in database
        #If match is found, skip it.  Otherwise, mark it as zombie.
	BK_DISK=`find $BACKUPPATH/ -maxdepth 3 -mindepth 3 | grep -v $BACKUPPATH/templates | grep -v $BACKUPPATH/db | grep -v database`
	BK_DB=`/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse "select identifier from backups"`

        if [ ! "$BK_DISK" ]; then
                echo -e "No backups in $BACKUPPATH/Exiting."
                graceful_exit
        fi

        if [ ! "$BK_DB" ]; then
                echo -e "${LTYELLOW}OnApp database query was empty.  Are there no backups?${NOCOLOR}Exiting."
                graceful_exit
        fi

        for disk in $BK_DISK; do
                found=false
                identifier=$(echo $disk |sed 's/.*\///g' | sed 's/\.[^\.]*$//') # Remove path, then also remove extension
                echo -ne "checking $disk...   \t"
		for db_row in $BK_DB; do
                        if [ $db_row == $identifier ]; then
                                echo -e "${GREEN}found, skip${NOCOLOR}"
                                found=true
                                break
                        fi
                done 
                if ! $found; then
                        echo -e "${YELLOW}not in db${NOCOLOR}"
                        echo $disk >> $TMPFILE

			#get size in bytes that will be removed
			if $size_reporting; then
				
				#normal backups are files:
				if [ -f $disk ]; then
					rmsize=$(ls -l $disk | cut -f 5 -d ' ')
				fi

				#incremental backups are directories
				#What makes them save space is hard-links.  
				#So we count the files with only 1 link 
				# http://stackoverflow.com/questions/19951883/du-counting-hardlinks-towards-filesize
				if [ -d $disk ]; then
					rmsize=$(find $disk/ -links 1 -print0 | du -cb --files0-from=- | tail -1 | awk '{print $1}')
				fi

			        : $[ totalsize = $totalsize + $rmsize ]

			fi #end size_reporting
                fi #end not found
        done
	if $size_reporting; then
		
		#if bc is there, use it for more precision
		if which bc > /dev/null 2>&1; then
			total_in_gb="$(echo "scale=2 ; $totalsize / 1024 / 1024 /1024" | bc)"
		else
			total_in_gb="$(( $totalsize / 1024 / 1024 /1024 ))"
		fi

        	echo "Total: $total_in_gb GB"
		
		echo -en "`date --utc +'%b %d %H:%M:%S (%Z)'`\t" >> /tmp/zbk_rmsize.txt
                echo "Total: $total_in_gb GB" >> /tmp/zbk_rmsize.txt
	
	fi
        echo ""
        echo "Done searching."
        echo ""
}


function find_zombies_remote()
{
        local ip=$1
        echo -e "${WHITE}Searching for zombies on $ip${NOCOLOR}"
	totalsize=0

        #For each backup on disk, check every identifier in database
        #If match is found, skip it.  Otherwise, mark it as zombie.
        BK_DISK=`ssh -i $SSHID root@$ip -p $SSHPORT find $BACKUPPATH/ -maxdepth 3 -mindepth 3 | grep -v $BACKUPPATH/templates | grep -v $BACKUPPATH/db | grep -v database`
        BK_DB=`/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse "select identifier from backups"`

	if [ ! "$BK_DISK" ]; then
		echo -e "No backups in $BACKUPPATH/Exiting."
		graceful_exit
	fi

	if [ ! "$BK_DB" ]; then
		echo -e "${LTYELLOW}OnApp database query was empty.  Are there no backups?${NOCOLOR}Exiting."
		graceful_exit
	fi

        for disk in $BK_DISK; do
                found=false
                identifier=$(echo $disk |sed 's/.*\///g' | sed 's/\.[^\.]*$//') # Remove path, then also remove extension
                echo -ne "checking $disk...   \t"
                for db_row in $BK_DB; do
                        if [ $db_row == $identifier ]; then
                                echo -e "${GREEN}found, skip${NOCOLOR}"
                                found=true
                                break
                        fi
                done
                if ! $found; then
                        echo -e "${YELLOW}not in db${NOCOLOR}"
                        echo $disk >> $TMPFILE

                        #get size in bytes that will be removed
                        if $size_reporting; then

				rmsize=$(ssh -i $SSHID root@$ip -p $SSHPORT "
                                	if [ -f $disk ]; then
                                        	ls -l $disk | cut -f 5 -d ' '
                                	fi

                                	if [ -d $disk ]; then
                                        	find $disk/ -links 1 -print0 | du -cb --files0-from=- | tail -1 | awk '{print \$1}'
                                	fi
				")

                                : $[ totalsize = $totalsize + $rmsize ]

                        fi #end size_reporting
                fi #end not found
        done
        if $size_reporting; then

                #if bc is installed, use it for more precision
                if which bc > /dev/null 2>&1; then
                        total_in_gb="$(echo "scale=2 ; $totalsize / 1024 / 1024 /1024" | bc)"
                else
                        total_in_gb="$(( $totalsize / 1024 / 1024 /1024 ))"
                fi

                echo "Total: $total_in_gb GB"
		
		echo -en "`date --utc +'%b %d %H:%M:%S (%Z)'`\t" >> /tmp/zbk_rmsize.txt
		echo "Total: $total_in_gb GB" >> /tmp/zbk_rmsize.txt
        
	fi
        echo ""
        echo "Done searching."
        echo ""
}

#For CP mode
function do_deletes()
{
    # Delete things in the list
    echo "Starting deletes"
    echo ""
    echo 127.0.0.1 >> $LOGFILE

    grep -vE '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' $TMPFILE | while read -r line || [[ -n $line ]]; do
        if [ -f $line ]; then
                echo -e "Deleting normal backup\t\t $line"
		#echo -e "`date --utc +'%b %d %H:%M:%S (%Z)'`\t" >> $BACKUPPATH/$RMLOG
                #rm -fv $line >> $BACKUPPATH/$RMLOG
		rm -f $line
		#echo "" >> $BACKUPPATH/$RMLOG
        fi
        if [ -d $line ]; then
                echo -e "Deleting incremental backup\t $line"
		#echo -e "`date --utc +'%b %d %H:%M:%S (%Z)'`\t" >> $BACKUPPATH/$RMLOG
                #rm -rfv $line >> $BACKUPPATH/$RMLOG
		rm -rf $line
		#echo "" >> $BACKUPPATH/$RMLOG
        fi

        if [ -e $line ]; then
                echo " FAILED TO DELETE: $line"
                echo "$line" >> $ERRORFILE
        else
                echo "$line" >> $LOGFILE
        fi
                 
    done

    rm $TMPFILE
    echo ""
    echo -e "${LTGREEN}Done.${NOCOLOR}"
    echo ""
}


#Takes validated IP as argument
function do_deletes_remote()
{
    local ip=$1

    # Copy list to remote server
    echo ""
    echo "Copying list to remote server"
    scp -i $SSHID -P $SSHPORT $TMPFILE root@${ip}:/tmp/.

    # Delete things in that list
    echo "Starting deletes"
    echo ""
    echo $ip >> $LOGFILE

    # useful article
    # http://stackoverflow.com/questions/13826149/how-have-both-local-and-remote-variable-inside-an-ssh-command
    ssh -i $SSHID root@$ip -p $SSHPORT "grep -vE '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' /tmp/$TMPFILE | while read -r line || [[ -n \$line ]]; do
        if [ -f \$line ]; then
                echo -e \"Deleting normal backup\t\t \$line\"
                rm -f \$line
        fi

        if [ -d \$line ]; then
                echo -e \"Deleting incremental backup\t \$line\"
                rm -rf \$line
        fi

        if [ -e \$line ]; then
                echo -n \" FAILED TO DELETE \$line\"
                echo \"\$line\" 1>&2
        else
                echo \"\$line\" >> /tmp/$LOGFILE
        fi

    done; rm /tmp/$TMPFILE" 2>> $ERRORFILE

        #If the error file has nothing in it, then remove it.
        if [ ! -s $ERRORFILE ]; then
                rm -f $ERRORFILE
        fi

        # Get the log file on the CP
        # I couldn't think of a way to use redirection and have a clean looking file
        # It would require a third file descriptor which won't work easily with ssh
        ssh -i $SSHID root@$ip -p $SSHPORT "cat /tmp/$LOGFILE ; rm /tmp/$LOGFILE" >> $LOGFILE

    rm $TMPFILE
    echo ""
    echo -e "${LTGREEN}Done.${NOCOLOR}"
    echo ""
}

function do_deletes_missing () 
{
    
    echo -e "\n${LTYELLOW}Starting deleting missing backups.${NOCOLOR}" >> $LOGFILE
    #local miss_list=$1
    local bid=($2)
    local del_count=0
    sleep 2s
    ##take a table backup before running delete
    read -p "Make a dump of the 'backups' table. Y/N?" bdump
    if [ ${bdump} = "Y" ] || [ ${bdump} = "y" ];then
        local BTABLE="backups"
        BDUMPPATH="`pwd`/${MYSQLDB}.${BTABLE}.`date +%Y-%m-%d-%H-%M`.sql"
        if /usr/bin/mysqldump -P"${MYSQLPORT}" -h"$MYSQLHOST" -u"${MYSQLUSER}" -p"${MYSQLPW}" "${MYSQLDB}" "${BTABLE}" > "${BDUMPPATH}";then
            echo -e "\nThe dump has been made to ${BDUMPPATH}" | tee -a $LOGFILE
        else 
            error_exit "Something went wrong: '=>do_delete_missing()'"; 
        fi
    fi
    ##Delete missing backups
    sleep 2s
    echo -e "\n${LTYELLOW} Removing missing backups...${NOCOLOR}\n" | tee -a $LOGFILE
    sleep 2s
    for b in ${bid[*]}; do
           if /usr/bin/mysql -P"${MYSQLPORT}" -h"$MYSQLHOST" -u"${MYSQLUSER}" -p"${MYSQLPW}" "${MYSQLDB}" -e "delete from backups where id=$b;";then
                echo -e "${GRAY}#${LTCYAN}${b}${NOCOLOR} ${LTYELLOW}deleted${NOCOLOR}" | tee -a $LOGFILE
                (( ++del_count ))
                sleep 1s
           else
                echo -e "Failed. Check ${ERRORFILE} for details"
                echo -e "\n${RED}Error while deleting backup id=$b:${NOCOLOR} '=>do_deletes_missing()'" | tee -a $ERRORFILE ; error_exit "${errcode}"
           fi
    done
    echo -e "\nSuccesfully deleted ${del_count} records.\n" | tee -a $LOGFILE
    graceful_exit
}

function do_cancel_zombie ()
{
echo -e "\n${LTYELLOW}Canceling zombie backup tranasactions...${NOCOLOR}" | tee -a $LOGFILE
    #local miss_list=$1
    local tids=($1)
    local tident=($2)
    local cancel_count=0
    sleep 2s
    local t_index=0
    for tid in ${tids[*]}; do
	    if /usr/bin/mysql -P"${MYSQLPORT}" -h"$MYSQLHOST" -u"${MYSQLUSER}" -p"${MYSQLPW}" "${MYSQLDB}" -e "update transactions set status='cancelled' \
		where parent_id=${tids[$t_index]} and identifier='"${tident[${t_index}]}"' and status='running';";then
		
		#echo "${tids[$t_index]}  ---  ${tident[${t_index}]}"	
			
		/usr/bin/mysql -P"${MYSQLPORT}" -h"$MYSQLHOST" -u"${MYSQLUSER}" -p"${MYSQLPW}" "${MYSQLDB}" -Bse "update log_items set status='cancelled' \
		where target_id in (select id from transactions where parent_id=${tids[$t_index]} and identifier='"${tident[${t_index}]}"');"
		#echo -e "${tids[$t_index]}  ${tident[$t_index]}"
		echo -e "Transaction ${GRAY}#${LTCYAN}${tid}${NOCOLOR} ${LTYELLOW}cancelled${NOCOLOR}" | tee -a $LOGFILE
                ((++cancel_count))
 		((++t_index))
                sleep 1s
        else
                echo -e "Failed. Check ${ERRORFILE} for details"
                echo -e "\n${RED}Error while cancelling backup transaction id=$tid:${NOCOLOR} '=>do_cancel_zombie()'" | tee -a $ERRORFILE ; error_exit "${errcode}"
        fi
    done
    echo -e "\nSuccesfully cancelled ${cancel_count} zombie transcation(s).\n" | tee -a $LOGFILE
    graceful_exit
}


function do_info_mode ()
{
    local pformat="%-25s ${CYAN}%-10s${NOCOLOR}\n"
    ONAPP_VER=$(rpm -q onapp-cp)
    ONAPP_VER=${ONAPP_VER%.*}
    printf "\n${WHITE}%s${NOCOLOR} %s\n" "OnApp CP version:" "${ONAPP_VER/#onapp-cp-/ }"
    
    printf "\n${YELLOW}%s${NOCOLOR}\n" "Backup Settings:"
     
    #Collect backup related configuration parameters
    
    BACKUPSSH=$(grep use_ssh_file_transfer /onapp/interface/config/on_app.yml | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g" | sed 's/\/$//g')
    BACKUPMODEPAR=($(grep allow_incremental_backups /onapp/interface/config/on_app.yml | grep -v grep))

    OIFS=$IFS
    IFS=$'\n'
    for i in $(egrep "simultaneous_backups|remove_backups_on_destroy_vm" /onapp/interface/config/on_app.yml | grep -v grep)
    do 
        BACKUPTRANPAR+=("$i")
    done
    #unset $i
    for j in $(egrep "rsync_option" /onapp/interface/config/on_app.yml | grep -v grep)
    do
	BACKUPRSYNC+=("$j")
    done
    IFS=$OIFS
   
    
    #Print gathered info
    printf "$pformat" "Backup path:"  "${BACKUPPATH["0"]}"
    printf "$pformat" "Allow incremental backups:"  "${BACKUPMODEPAR["1"]}"
    printf "$pformat" "Use SSH File Transfer:" "${BACKUPSSH}"
    
    printf "\n\n%s${WHITE}Rsync options:${NOCOLOR}\n"
    for rsc in "${BACKUPRSYNC[@]}"
    do
        rsc_name=$(echo ${rsc} | awk '{print $1}' | sed -n 's/\_/ /pg')
        rsc_value=$(echo ${rsc} | awk '{print $2}')
        printf "%s ${CYAN}%s${NOCOLOR}\n" "${rsc_name^}" "${rsc_value}"
    done  
    printf "\n\n${WHITE}Backup Processes:${NOCOLOR}\n"
    for tp in "${BACKUPTRANPAR[@]}"
    do
	tp_name=$(echo ${tp} | awk '{print $1}' | sed -n 's/\_/ /pg')
	tp_value=$(echo ${tp} | awk '{print $2}')
	printf "%s ${CYAN}%s${NOCOLOR}\n" "${tp_name^}" "${tp_value}"
    done
    echo ""
     

#Collect information from the DB
  
#Get Runnig backup transactions
   echo -en "${WHITE}Runnig backup transactions: $NOCOLOR"
   while read rbkid rbkidentifier; do
                runningids+=("$rbkid")
                runningidentifiers+=("$rbkidentifier")
                foundprocesses+=("false")
   done < <(/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse "select parent_id,identifier from transactions where status='running' and (action='take_backup' or action='take_incremental_backup');")

#If no running backups in UI, just continue
        if [ ! "${runningids}" ]; then 
                echo -e "${LTGREEN}No running backup transactions found.${NOCOLOR}"
        else
		echo -e "${LTGREEN}${#runningids[@]} running backup transaction(s) found.${NOCOLOR}"
		echo -e "\n${YELLOW}Run sript with '-p' option to check they are not zombie actually${NOCOLOR}"
        fi
  echo ""

#List of backup server and some statistic
  echo -e "\n${WHITE}Backup servers statistics:$NOCOLOR"   
  while read bsid bsip bcap bused bnumber; do
 	bsids+=("$bsid")
	bsips+=("$bsip")
	bcaps+=("$bcap")
	buseds+=("$bused")
    bnumbers+=("$bnumber")
   done < <(/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse 'select bs.id as ID,bs.ip_address as "IP addr",bs.capacity \
	as "Capacity,GB",sum(b.backup_size)/1024/1024 as "Consumed", count(b.id) as "Backups, N" from backup_servers bs left join backups b on bs.id=b.backup_server_id where bs.ip_address is not null \
	and bs.enabled=1 group by bs.id;')

if [[ $bsids -gt 0 ]]; then
row=0
printf "${LTCYAN}%s\t%s\t\t%s\t\t%s\t%s\n${NOCOLOR}" "|ID" "|IP" "|Cap,GB" "|Used,GB" "|Backups,N"
for b in "${bsids[@]}"
do    
     bused_f=${buseds[$row]}
     if [[ $bused_f = 'NULL' ]];then bused_f=0;fi
     printf "%s\t%s\t%s\t\t%.2f\t\t%s\n" "${bsids[$row]}" "${bsips[$row]}" "${bcaps[$row]}" "${bused_f}" "${bnumbers[$row]}"
     ((++row))
done   
else
    printf "${LTYELLOW}No backup servers found in the cloud${NOCOLOR}"
fi

echo ""

#Check if backups were taken on the HVs 
echo -e "\n${WHITE}Hypervisors statistics:$NOCOLOR"   
while read bnumber bsize; do
    h_bnumbers+=("$bnumber")
    h_bsizes+=("$bsize")
   done < <(/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse 'select count(b.id), sum(b.backup_size)/1024/1024 \
   from backups b where b.built=1 and b.backup_server_id is null;')

if [[ $h_bnumber -gt 0 ]] && [[ $bsids -gt 0 ]]; then
         printf "Some backups were taken on the HVs\n"
row=0
printf "${LTCYAN}%s\t\t%s\n${NOCOLOR}" "|Total,GB" "|Backups,N"
for b in "${bids[@]}"
do    
     bused_f=${buseds[$row]}
     if [[ $bused_f = 'NULL' ]];then bused_f=0;fi
     printf "%s\t%s\t\t%.2f\t\t%s\n" "${bids[$row]}" "${bsips[$row]}" "${bcaps[$row]}" "${bused_f}" "${bnumbers[$row]}"
     ((++row))
done   
else
    printf "${LTYELLOW}No backups were taken on the HVs\n${NOCOLOR}"
fi
echo ""
printf "${LTMAGENTA}To be continued${NOCOLOR}\n"

#To do:
#Failed schecdules ... 

   graceful_exit
}


##############
# Trap signals
##############
trap "signal_exit TERM" TERM HUP
trap "signal_exit INT"  INT


#################
# PARAMETER STUFF
################

#http://stackoverflow.com/questions/17752204/is-there-a-way-to-force-a-shell-script-to-run-under-bash-instead-of-sh
if ! echo $CURR_INTERPRETER | grep bash > /dev/null 2>&1; then
	error_exit "You must use bash"
fi


if [ $# -lt 1 ]; then
        usage
        error_exit "Options are required.  Use -h for help."
fi

bs_mode=0
cp_mode=0
hv_mode=0
remove=0
proc_mode=false
missing_mode=false
size_reporting=false
info_mode=false
unset name  # zol

# long options are NOT supported by getopts, would need to use getopt instead
  while getopts ":hCB:H:dipsM" option; do
    case ${option} in
      h | help) help_message && graceful_exit ;;
      B | backupserver) ((++remove));bs_mode=1; bk_addr=${OPTARG} ;;
      C | controlpanel) ((++remove));cp_mode=1;;
      H | hypervisor) ((++remove));hv_mode=1; hv_addr=${OPTARG} ;;
      d | debug) debug=true;;
      p | process) proc_mode=true;;
      s | size_reporting) size_reporting=true;;
	  M | missing_backups) missing_mode=true;;
      i | I) info_mode=true ;;
      : ) echo "Argument is required, try -h for usage." && graceful_exit ;;
      ? ) echo "Invalid option "$@", try -h for usage." && graceful_exit ;; # zol
      - ) shift $(( ${OPTIND} - 1 )); break ;;
      * ) echo -e "Invalid option/argument '${OPTARG}', try -h for usage." && graceful_exit ;;
    esac
  done

# echo "name is ${OPTIND}, Err ${OPTERR}, ${OPTARG}"
# # zol; if no applicable options specified then exit
# if [ -z "$name" ]; then
#     echo "Options were NOT given, or they are unsupported, try -h for usage."
#     usage
#     graceful_exit
# fi

shift $(( ${OPTIND} - 1 ))


# Make sure only one of the removal parameters was chosen
if [ $remove -gt 1 ]; then
        error_exit "Too many parameters.Choose a single location for removing backups"
        usage
fi

#########
# SETUP
#########

# Check if necessary config files exist
# If not, we're probably not on a CP.

cp=1

if ! ls /onapp/interface/config/on_app.yml > /dev/null 2>&1; then
        echo "/onapp/interface/config/on_app.yml not found"
        cp=0
fi

if ! ls /onapp/interface/config/database.yml > /dev/null 2>&1; then
        echo "/onapp/interface/config/database.yml not found"        
        cp=0
fi

#Does OnApp user exist?
if id -u onapp > /dev/null 2>&1; then
        sshuser=onapp
else
        echo "No onapp user!"
        cp=0
        sshuser=root
fi

if [ $cp -eq 0 ]; then
        echo -e "Warning!  This script should be run on an OnApp Control Panel server."

        #If we're not debugging, then this is an error
        #so exit
        if ! $debug; then
                echo -e "Exiting."
                graceful_exit
        fi
fi

#Get parameters for ssh and mysql
SSHPORT=$(grep ssh_port /onapp/interface/config/on_app.yml | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g")
SSHID=$(eval echo ~$sshuser/.ssh/id_rsa)
MYSQLHOST=$(cat /onapp/interface/config/database.yml | grep host | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g")
MYSQLPORT=$(cat /onapp/interface/config/database.yml | grep port | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g")
MYSQLDB=$(cat /onapp/interface/config/database.yml | grep database | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g")
MYSQLPW=$(cat /onapp/interface/config/database.yml | grep password | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g")
MYSQLUSER=$(cat /onapp/interface/config/database.yml | grep user | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g")

#Backup path with trailing slash removed
BACKUPPATH=$(grep backups_path /onapp/interface/config/on_app.yml | head -1 | awk '{print $2}' | sed "s/'$//g;s/^'//g;s/\"$//g;s/^\"//g" | sed 's/\/$//g')

TMPFILE=to_remove.txt; #if [ -e ${TMPFILE} ] echo '' > ${TMPFILE}
LOGFILE=removed_backups.txt ; #echo '' > ${LOGFILE}
ERRORFILE=failed_to_remove.txt; #echo '' > ${ERRORFILE}
RMLOG=zbk_rm.log

if $debug; then
        echo ""
        echo "#############"
        echo "# DEBUG MODE!"
        echo "#############"
        echo ""
        echo -e "VARS--------------"
	echo "TMPFILE $TMPFILE"
	echo "LOGFILE $LOGFILE"
	echo "ERRORFILE $ERRORFILE"
	echo "RMLOG $RMLOG"
	echo ""
        echo "SSH PORT $SSHPORT"
        echo "SSH ID $SSHID"
        echo "MYSQLHOST $MYSQLHOST"
	echo "MYSQLPORT $MYSQLPORT"
        echo "MYSQLDB $MYSQLDB"
        echo "MYSQLPW $MYSQLPW"
        echo "BACKUPPATH $BACKUPPATH"
	echo ""
	echo "cp $cp"
	echo "bs_mode $bs_mode"
	echo "cp_mode $cp_mode"
	echo "hv_mode $hv_mode"
	echo "remove $remove"
	echo "proc_mode $proc_mode"
	echo "missing_mode $missing_mode"
	echo "size_reporting $size_reporting"
        echo ""
        #echo "Exiting"
        graceful_exit
fi

if $info_mode; then

	do_info_mode 
fi
##############
# Main Logic #
##############

#Zombie backups are defined to be backups on disk that don't exist in the database

# 1) Find zombie backups
# 2) Put them in TMPFILE
# 3) Delete them
# 4) Log deleted files in LOGFILE

# 1 and 2 done in function "find_zombies"
# 3 and 4 done in function "do_deletes"

if [ $remove -eq 1 ]; then

        ## Backup Server mode
        #Validate IP and find zombies
        if [ $bs_mode -eq 1 ]; then
                echo "List of backup servers:"
                BK_LIST=`/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse 'select ip_address from backup_servers where ip_address is not NULL;'`

                echo $BK_LIST
                echo ""

                bk_entered=0
                echo $bk_addr > $TMPFILE

                if valid_ip $bk_addr; then
                        for i in $BK_LIST; do
                                if [ $i == $bk_addr ]; then 
                                        bk_entered=1
                                        echo "$bk_addr valid"
                                        echo ""
                                fi
                        done
                else
                        echo "$bk_addr is not a valid ip address"
			graceful_exit
                fi

                if [ $bk_entered -eq 0 ]; then 
			echo -e "${YELLOW}$bk_addr is NOT listed in OnApp as a backup server.${NOCOLOR}"

                        while true; do
				echo -e "${CYAN}Continue anyway?${NOCOLOR}"
                                read bsyn
                                case $bsyn in
                                        [Yy]* ) break;;
                                        [Nn]* ) echo "Exiting"; graceful_exit;;
                                        * ) echo "Please answer y or n: ";;
                                esac
                        done
		fi

                find_zombies_remote $bk_addr
        fi #end bs_mode
        
        ## Hypervisor mode
        #Validate IP and find zombies
        if [ $hv_mode -eq 1 ]; then
                echo "List of hypervisors:"
                HV_LIST=`/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse 'select ip_address from hypervisors where ip_address is not NULL;'`

                echo $HV_LIST
                echo ""

                hv_entered=0
                echo $hv_addr > $TMPFILE

                if valid_ip $hv_addr; then
                        for i in $HV_LIST; do
                                if [ $i == $hv_addr ]; then 
                                        hv_entered=1 
                                        echo "$hv_addr valid"
                                        echo ""
                                fi
                        done
                else
                        echo "$hv_addr is not a valid ip address"
                fi

                if [ $hv_entered -eq 0 ]; then echo "$hv_addr is not listed in OnApp as a hypervisor"; graceful_exit; fi

                find_zombies_remote $hv_addr
        fi # end hv_mode

        ## Control Panel mode
        # Find zombies
        if [ $cp_mode -eq 1 ]; then
                echo "127.0.0.1" > $TMPFILE
                find_zombies
        fi


        # If there were no zombies found, then we are done!
        # Report result to user and exit
        num_zombie=$(grep -vE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" $TMPFILE | wc -l)
        if [ $num_zombie -eq 0 ]; then
                echo "" 
                echo -e "${LTGREEN}No zombies found!${NOCOLOR}"
                graceful_exit
        fi

        # Otherwise
        # Confirm list of zombies with user
        echo ""
        echo "MARKED FOR DELETION"
        echo "-------------------"
        grep -vE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" $TMPFILE | while read -r line || [[ -n $line ]]; do
            echo "$line"
        done
        echo ""

        # If they say it looks good, do deletes
        echo "Please review the above list."
        while true; do
                read -p "Do you wish to delete everything in this list (cannot be undone)?" yn
                case $yn in
                        [Yy]* ) if [ $cp_mode -eq 1 ]; then do_deletes; fi 
                                if [ $bs_mode -eq 1 ]; then do_deletes_remote $bk_addr; fi
                                if [ $hv_mode -eq 1 ]; then do_deletes_remote $hv_addr; fi
                                break;;
                        [Nn]* ) echo "Exiting"; rm -f $LOCKFILE; exit;; #exit without deleting TMPFILE
                        * ) echo "Please answer y or n: ";;
        esac
        done

        graceful_exit #Will prevent proc mode from also running
fi #end if (remove=1)


### PROCESS MODE
#
# Find zombie UI tasks
# 
###

# http://stackoverflow.com/questions/7612320/bash-weird-variable-scope-when-populating-array-with-results

if $proc_mode; then

	zombiesfound=false

	#Get running backup information
        while read rbkid rbkidentifier; do
                runningids+=("$rbkid")
                runningidentifiers+=("$rbkidentifier")
                foundprocesses+=("false")
        done < <(/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse "select parent_id,identifier from transactions where status='running' and (action='take_backup' or action='take_incremental_backup');")

	#If no running backups in UI, just exit
	if [ ! "${runningids}" ]; then #Zol if [ ! "$runningids" ]
		echo -e "${LTGREEN}No running backup transactions found.${NOCOLOR}"
		graceful_exit
	fi

        BK_LIST=`/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse 'select ip_address from backup_servers where ip_address is not null and enabled=1;'`

        #If backup servers exist, check on them
        if [ "$BK_LIST" ]; then 
                for ip in ${BK_LIST[*]}; do
			echo -e "${WHITE}Checking backup processes on BS $ip${NOCOLOR}"
			echo "--------------------------------------------"
                        result+=( $(ssh -i $SSHID root@$ip -p $SSHPORT "
				unset bkidentifiers; 
				bkidentifiers=(${runningidentifiers[*]});
				for b in \${bkidentifiers[*]}; do
					if ps aux | grep \$b | grep -v grep > /dev/null 2>&1; then
						echo "true";
					else
						echo "false";
					fi
				done 
			") )
			
			#debug output--to check that result returns something sensible
		        #echo result: 
		        #for a in ${!result[*]}; do
		        #        echo $a : ${result[$a]}
		        #done

                        #Output what is found to user
                        for a in ${!result[*]}; do
                                if ${result[$a]}; then
                                        echo -e "Backup #${CYAN}${runningids[$a]}${NOCOLOR} (${runningidentifiers[$a]}) running"
                                fi
                        done
			echo ""

			#  Merge result and foundprocesses
			for j in ${!result[*]}; do
				if ${result[$j]}; then
					foundprocesses[$j]=true
				fi
			done
			unset result	
		done

        else #No backups servers- check HVs
                echo "No backup servers found!"
		HV_LIST=`/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse 'select ip_address from hypervisors where ip_address is not null and online=1;'`

        	#Probably unnecessary, but adding check just in case
        	if [ ! "$HV_LIST" ]; then
			echo "No hypervisors found!  Exiting."
			graceful_exit
		fi 

		echo ""
                for ip in ${HV_LIST[*]}; do
			echo -e "${WHITE}Checking backup processes on HV $ip${NOCOLOR}"
			echo "-------------------------------------------"
                        result+=( $(ssh -i $SSHID root@$ip -p $SSHPORT "
				unset bkidentifiers; 
				bkidentifiers=(${runningidentifiers[*]});
				for b in \${bkidentifiers[*]}; do
					if ps aux | grep \$b | grep -v grep > /dev/null 2>&1; then
						echo "true";
					else
						echo "false";
					fi
				done 
			") )
			
			#debug output--to check that result returns something sensible
		        #echo result: 
		        #for a in ${!result[*]}; do
		        #        echo $a : ${result[$a]}
		        #done

                        #Output what is found to user
                        for a in ${!result[*]}; do
                                if ${result[$a]}; then
                                        echo "Backup #${runningids[$a]} (${runningidentifiers[$a]}) running"
                                fi
                        done
			echo ""

			#  Merge result and foundprocesses
			for j in ${!result[*]}; do
				if ${result[$j]}; then
					foundprocesses[$j]=true
				fi
			done
			unset result
		done
        fi #endif BS/HV exist

	echo "Done checking."
	echo ""

        #debug final result
	#echo "output array contents"
        #for i in ${!foundprocesses[*]}; do
        #        echo "$i : ${runningids[$i]} : ${runningidentifiers[$i]} : ${foundprocesses[$i]}"
        #done

        #were there zombies?
        for i in ${!foundprocesses[*]}; do
		if ! ${foundprocesses[$i]}; then
			zombiesfound=true
		fi
        done
	
	#output which ones are zombies
	if ! $zombiesfound; then
		echo -e "${LTGREEN}No zombie transactions found.${NOCOLOR}"
		graceful_exit
	else
		echo -e "${YELLOW}ZOMBIE TRANSACTIONS FOUND${NOCOLOR}"
		echo -e "Backup#\t\tIdentifier"        
       	 	for i in ${!foundprocesses[*]}; do
                	if ! ${foundprocesses[$i]}; then
				runningidstocancel+=("${runningids[$i]}") #Zol
				runningidentifierstocancel+=("${runningidentifiers[$i]}") #Zol
				echo -e "${runningids[$i]}\t\t${runningidentifiers[$i]}"
			fi
       		done
	    ## Zol; 
            ## Ask users about cancel zombie transactions:
            if [[ ${#runningidstocancel[*]} -ne 0 ]]; then
            echo -e "${WHITE}Do you wish to cancel these #${GREEN}${#runningidstocancel[*]}${NOCOLOR} ${WHITE}zombie backup transaction(s)?${NOCOLOR}"
            while true; do
                    read -p "Y/N?" yn
                    case $yn in
                            Y|y ) do_cancel_zombie "${runningidstocancel[*]}" "${runningidentifierstocancel[*]}" && graceful_exit;;
                            N|n ) echo -e "${GREEN}Done.${NOCOLOR}";break ;;
                            * ) echo "Yes or NO?" ;;
                    esac
            done
             #   else echo -e "\nNo missing backups found" | fee -a "${LOGFILE}";echo ""
            fi
            ## end Zol

		echo ""
		echo "Please re-run this script in 3 minutes."  
		echo "If the transactions are still zombies, then cancel them from the web UI."
		echo ""
	fi
fi #endif proc mode

### MISSING BACKUP MODE
#
# Find backups without actual files
# 
###


if $missing_mode; then

	#logic
	#for each backup server with backups
	#((null will be possible value for backup server--check HVs for these backups))
		#check files on that bs 
		### TO DO if backup has backup_server_id for backup server which does not exist, this script won't tell you about it
			#print those not found (id and identifier since id needed for deleting from CP's API)
		
	BK_LIST=`/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse 'select distinct bs.ip_address from backups b left join backup_servers bs on b.backup_server_id=bs.id order by bs.ip_address desc;'`

    #If backup servers exist, check on them
    if [ "$BK_LIST" ]; then
		
		
		echo -e "${LTYELLOW}Warning:${NOCOLOR}  First make sure that backup storage is mounted on the Backup Server(s)";
		echo -e "${YELLOW}This script will show false positives if storage is not mounted!${NOCOLOR}";
		
		echo -e "${CYAN}Running missing backup check in a few moments...${NOCOLOR}" | tee -a "${LOGFILE}";
		sleep 5s;
	    for ip in ${BK_LIST[*]}; do
			
            #be sure the backup storage is mounted on the BS.
            ismounted=$(ssh -i $SSHID root@$ip -p $SSHPORT "[ ! -d $BACKUPPATH ] && echo \"false\" || echo \"true\"")

            if [ ${ismounted} = "false" ]; then 
                echo -e "\n${YELLOW}The backup storage is not mounted on the Backup Server ${ip}! Skipping this BS...${NOCOLOR}" | tee -a "${LOGFILE}";
                continue;
			fi
            #end of ismounted check

            ### TO DO
			### if null should check HVs  Would need to follow form/function from proc mode when it checks HVs
			###  (note that "ip_address=NULL" won't work in mysql, needs to be "ip_address is NULL")
			if [ "$ip" = "NULL" ]; then
				echo -e "\n${YELLOW}Some backups made it to Compute Resources (HVs) instead of the Backup Server(s)${NOCOLOR}" | tee -a "${LOGFILE}"
				continue;
			fi ### when implementing HV check, change this "fi" to "else"
			

			#Get list of backups which should exist on this bs
			while read bkid bkidentifier; do
                bkids+=("$bkid")
                bkidentifiers+=("$bkidentifier")
			done < <(/usr/bin/mysql -P $MYSQLPORT -h $MYSQLHOST $MYSQLDB -u $MYSQLUSER -p$MYSQLPW -Bse "select b.id,b.identifier from backups b left join backup_servers bs on b.backup_server_id=bs.id where b.built=1 and bs.ip_address='$ip';")
			
						echo -e "\n${WHITE}Checking for missing backups on $ip${NOCOLOR}" | tee -a "${LOGFILE}"
			echo "--------------------------------------------" 
			#true means missing, false means exists
            missing_list+=( $(ssh -i $SSHID root@$ip -p $SSHPORT "
				unset remotebkidentifiers; 
				remotebkidentifiers=(${bkidentifiers[*]});
				for b in \${remotebkidentifiers[*]}; do
					if [ -e \"$BACKUPPATH/\${b:0:1}/\${b:1:1}/\$b\" ]; then
						if [ -z \"\$(ls -A $BACKUPPATH/\${b:0:1}/\${b:1:1}/\$b)\" ]; then
							echo \"true\";
						else
							echo \"false\";
						fi
					else
							echo \"true\";
					fi
				done 
				") )
				
			#Output what is found to user
            # zol; creating an array of bakups to remove
			missing_count=0
            for a in ${!missing_list[*]}; do
                if ${missing_list[$a]}; then
                    (( ++missing_count ))  #zol
                    missing_id_to_remove+=("${bkids[a]}") #zol
	            echo -e "Backup #${CYAN}${bkids[$a]}${NOCOLOR} (${bkidentifiers[$a]}) ${YELLOW}missing${NOCOLOR}"
                fi
            done
	    echo ""
			
            ## Zol; 
            ## Ask users about deletion missing backups:
            if [[ ${missing_count} -gt 0 ]]; then
            echo -e "${WHITE}Do you wish to delete these ${GREEN}${missing_count} missing backup rescords?${NOCOLOR}"
            while true; do
                    read -p "Y/N?" yn
                    case $yn in
                            Y|y ) do_deletes_missing "${missing_list[*]}" "${missing_id_to_remove[*]}" && graceful_exit;;
                            N|n ) echo -e "${GREEN}Done.${NOCOLOR}"; graceful_exit ;;
                            * ) echo "Please answer y or n: " ;;
                    esac
            done
                else echo -e "\nNo missing backups found" | tee -a "${LOGFILE}";echo ""
            fi
            ## end Zol

			unset bkids;
			unset bkidentifiers;
			unset missing_list;
			
		done #end loop for each BS

        echo -e "${YELLOW}No backups records found in the DB.${NOCOLOR}"
	
    fi #endif for "BS list not empty"
	
	echo -e "${LTGREEN}Done.${NOCOLOR}"
	
fi #endif missing_mode

#start info_mode

graceful_exit
