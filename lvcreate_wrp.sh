#!/usr/bin/env bash

# lvcreate wrapper
# During snapshot creation calculates the real disk usage inside the VM and replaces the '-L' value
# Requires the existence of a separate /tmp/lvcreate symlink to lvm utility
# ln -s /usr/sbin/lvm /tmp/lvcreate

# Example backup command
#lvcreate -s -L600GB -n backup-wzhsqyrknpfjmb /dev/onapp-dscwwsrczqsslj/vmbbckobistfrg\

ORIGIN=${BASH_ARGV[0]} # /dev/onapp-dscwwsrczqsslj/vmbbckobistfrg
SNAPSHOT_NAME=${BASH_ARGV[1]} # backup-wzhsqyrknpfjmb

DISK_NAME=$(lvdisplay "${ORIGIN}" | awk '/LV Name/ {print $3}') # vmbbckobistfrg

# We are using virt-df to calculate the real disk usage inside the VM
# In our exanple the real usage is 1.4G
FS_SIZE=$(virt-df -h "${ORIGIN}" | awk -v pattern="${DISK_NAME}" '$0 ~ pattern {print $3}' | cut -d "." -f 1) # 1
SNAPSHOT_SIZE=$((FS_SIZE + 10)) # 11; adding 10 extra GB to the snapshot, just in case

#/tmp/lvcreate is a symlink to /usr/sbin/lvm utility
# If there is no '-s' flag then lvcreate is invoked with the original parameters
if [ "${BASH_ARGV[4]}" == "-s" ]
then
  /tmp/lvcreate -s -L "${SNAPSHOT_SIZE}G" -n "${SNAPSHOT_NAME}" "${ORIGIN}"
else
  /tmp/lvcreate $@
fi