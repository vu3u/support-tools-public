#!/usr/bin/bash

# The script is for truncating old hourly stats in partitioned tables. 
# billing_usage_statistics, billing_statistics_hourly
# A.G & A.K 

### INIT() ###

[ -d '/onapp/interface/' ] || exit 1

#Log file
LOG_FILE=/onapp/interface/log/archive_hourly_stats_custom-$(date +%F).log

SCRIPTNAME=${0##*/}
echo -e "$(date)\n${SCRIPTNAME} started\n" > $LOG_FILE

#Get Number of mounths to keep stats
STATS_KEEP_MONTH=`awk '$1=="archive_stats_period:"{print $2}' /onapp/interface/config/on_app.yml`

#STATS_KEEP_MONTH=5 # for testing
echo -e "Delete all records older than $STATS_KEEP_MONTH mounth(s)\n" >> $LOG_FILE

if [ "$STATS_KEEP_MONTH" -gt 10 ]; then
  echo -e "The statistic keep period is greater than 10 months. Processing for such a period isn't supported\n"  >> $LOG_FILE
  exit 1
fi

# Add 1 to get whole month.
((STATS_KEEP_MONTH++))

# Getting credentials from database.yml
DB_CONF="/onapp/interface/config/database.yml"
DB_NAME=`awk '$1=="database:"{print $2}' $DB_CONF`
DB_USER=`awk '$1=="username:"{print $2}' $DB_CONF`
DB_PASS=`awk '$1=="password:"{print $2}' $DB_CONF`
DB_HOST=`awk '$1=="host:"{print $2}' $DB_CONF`
DB_PORT=`awk '$1=="port:"{print $2}' $DB_CONF`

### MAIN() ###

Get_Part_Rows(){
 T_Name=$1; P_Name=$2
 mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -Bse "SELECT TABLE_ROWS FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_NAME = '$T_Name' and PARTITION_NAME='$P_Name';" 2>/dev/null
}

Truncate_Partition(){
#Options: table_name partition_name Mode
 T_Name=$1; P_Name=$2 Mode=$3
 P_Rows=`Get_Part_Rows $T_Name $P_Name`
   if [ "$P_Rows" -ne 0 ]; then  # truncate partition only if it has records.
      if [ "X$Mode" != "Xdry" ]; then
          echo -e "ALTER TABLE $T_Name TRUNCATE PARTITION $P_Name;" >> $LOG_FILE
          mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -Bse "ALTER TABLE $T_Name TRUNCATE PARTITION $P_Name;" >>$LOG_FILE 2>/dev/null
      else
          echo -e "ALTER TABLE $T_Name TRUNCATE PARTITION $P_Name;" >> $LOG_FILE
      fi
    else
       echo -e "$T_Name: $P_Name skipped (zero records)" >> $LOG_FILE
   fi
}

if [ "X$1" == "Xdry" ]; then
  echo -e "This is the dry mode. No changes to the DB\n" >> $LOG_FILE
else
  echo -e "Executing the queries in DB:\n" >> $LOG_FILE
fi

for (( c=$STATS_KEEP_MONTH; c<=11; c++ ))
do
   PART2TRUN=P`date -d "-$c month" +%m`
   Truncate_Partition "billing_usage_statistics" $PART2TRUN $1
   Truncate_Partition "billing_statistics_hourly" $PART2TRUN $1
done

echo >> $LOG_FILE
echo -e "Table's status after the cleanup:\n" >> $LOG_FILE

mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -e "SELECT TABLE_NAME,PARTITION_NAME,TABLE_ROWS FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_NAME = 'billing_usage_statistics';" >> $LOG_FILE  2>/dev/null
echo >> $LOG_FILE
mysql --host=$DB_HOST --port=$DB_PORT -u$DB_USER -p$DB_PASS $DB_NAME -e "SELECT TABLE_NAME,PARTITION_NAME,TABLE_ROWS FROM INFORMATION_SCHEMA.PARTITIONS WHERE TABLE_NAME = 'billing_statistics_hourly';" >> $LOG_FILE 2>/dev/null

exit 0
