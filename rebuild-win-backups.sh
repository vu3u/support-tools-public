#!/usr/bin/env bash

#For very limited use only!

#It helps to rebuild the Windows backups to be compatible with new feature Force Windows Backup
#It goes through all existing Windows backups and processes only ones that were taken with ntfsclone wrapper

#Author andriy.kuzma@onapp.com

#version 0.2, 2022

LOCKFILE=/tmp/r-w-b.lock

#Make sure only one instance of the script is running
if [ -e $LOCKFILE ]; then
        echo "The script is already running. ($LOCKFILE found)"
        echo Please allow it to finish before starting another instance.
	echo Exiting.
        exit
fi
touch $LOCKFILE


#DB info 
DBCFG="/onapp/interface/config/database.yml"
DBNAME=$(awk '$1=="database:"{print $2}' $DBCFG)
DBPASS=$(awk '$1=="password:"{print $2}' $DBCFG)
DBUSER=$(awk '$1=="username:"{print $2}' $DBCFG)
DBHOST=$(awk '$1=="host:"{print $2}' $DBCFG)
DBPORT=$(awk '$1=="port:"{print $2}' $DBCFG)

#SSH
SSHKEY="/onapp/interface/config/keys/private"

#LOG
LOGFILE="/tmp/r-w-b-$(date +%F_%H-%M-%S).log"
rm -f $LOGFILE
touch $LOGFILE
echo -e "Log file: $LOGFILE"

#List of processed backups
BKPLIST="/tmp/r-w-b-list-$(date +%F_%H-%M-%S).txt"
rm -f $BKPLIST
touch $BKPLIST
echo -e "List of processed backups: $BKPLIST"
echo 

#BKPPATH
BKPPATH=$(awk '$1=="backups_path:"{print $2}' /onapp/interface/config/on_app.yml | sed 's/\"//g')


#Get a list of Windows backups and their details

#BACKUPS_DATA=$(mysql --host=$DBHOST --port=$DBPORT -u $DBUSER -p$DBPASS $DBNAME -Bse \
#"select b.identifier,b.min_disk_size,b.backup_size from backups b where b.built=1 and b.operating_system='windows';" 2>/dev/null)

#read bidentifier bminsize bsize <<< $(echo $BACKUPS_DATA)

graceful_exit() {
        rm -f $LOCKFILE
        exit
}

recompose_backup()
{

local bidentifier=$1
local mindisksize=$2
local bbackupsize=$3
local bsip=$4



full_bkp_path=$(eval echo "$BKPPATH/${bidentifier:0:1}/${bidentifier:1:1}/$bidentifier")

echo -e "Processing $full_bkp_path on $bsip" | tee -a $LOGFILE
echo -e "Min disk size: ${mindisksize}GB" | tee -a $LOGFILE
echo -e "Backup size: ${bbackupsize}KB" | tee -a $LOGFIL

if ssh -n -q -i $SSHKEY root@$bsip "file $full_bkp_path | grep 'gzip compressed data' && test -e $full_bkp_path.mbr && test ! -e $full_bkp_path.origin"
then
ssh -T -i $SSHKEY root@$bsip << COMMANDS >> $LOGFILE 2>&1
echo -e "Step 1: mv $full_bkp_path{,.origin}"
mv $full_bkp_path{,.origin}
echo -e "Step 2: dd if=$full_bkp_path.mbr of=$full_bkp_path.img"
dd if=$full_bkp_path.mbr of=$full_bkp_path.img 
echo -e "Step 3: pigz -d -c $full_bkp_path.origin | dd seek=1 bs=1M of=$full_bkp_path.img"
pigz -d -c $full_bkp_path.origin | dd seek=1 bs=1M of=$full_bkp_path.img
echo -e "Cheking the file system is accesible" 
echo -e "Step 4: losetup --partscan --find --show $full_bkp_path.img" 
ldev=\$(losetup --partscan --find --show $full_bkp_path.img)
echo -e "Step 5: ntfsfix -d \${ldev}p1" 
ntfsfix -d \${ldev}p1
echo -e "Step 6: mkdir -p /mnt/$bidentifier"
mkdir -p /mnt/$bidentifier
echo -e "Step 7: mount  \${ldev}p1 /mnt/$bidentifier"
mount \${ldev}p1 /mnt/$bidentifier
echo -e "Step 8: ls /mnt/$bidentifier/Windows/explorer.exe"
ls /mnt/$bidentifier/Windows/explorer.exe && echo -e "FS check passed" || echo -e "FS check failed for $bidentifier"
echo -e "Step 9: umount \${ldev}p1 && losetup -d \${ldev}" 
umount \${ldev}p1 && losetup -d \${ldev}
echo -e "Step 10: Compressing => pigz < $full_bkp_path.img > $full_bkp_path"
pigz < $full_bkp_path.img > $full_bkp_path
echo -e "Step 11: rm -f $full_bkp_path.img" 
rm -f $full_bkp_path.img
echo -e "  rm -f $full_bkp_path.origin - skipped"
#rm -f $full_bkp_path.origin
COMMANDS

echo -e "$bsip $full_bkp_path" >> $BKPLIST

else
	echo "$bidentifier - skipped, the backup does not meet conditions" | tee -a $LOGFILE
fi

echo -e "\n\n" | tee -a $LOGFILE

}

#Get a number of all windows backups
NBKPS=$(mysql --host=$DBHOST --port=$DBPORT -u $DBUSER -p$DBPASS $DBNAME -Bse \
	"select count(b.identifier) from backups b join backup_servers bs on b.backup_server_id=bs.id where b.built='1' and b.operating_system='windows' and bs.enabled='1';")

echo -e "Found Windows backups to process: $NBKPS\n" | tee -a $LOGFILE

#Get backup identifier, its size, and backup server the backup located on
while read b_identifier b_min_disk_size b_backup_size b_bs_ip
do
	recompose_backup $b_identifier $b_min_disk_size $b_backup_size $b_bs_ip

done < <(mysql --host=$DBHOST --port=$DBPORT -u $DBUSER -p$DBPASS $DBNAME -Bse \
	"select b.identifier,b.min_disk_size,b.backup_size,bs.ip_address from backups b join backup_servers bs on b.backup_server_id=bs.id where b.built='1' and b.operating_system='windows' and bs.enabled='1';")

graceful_exit